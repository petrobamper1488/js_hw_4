let x = +prompt("Введіть будь-яке число х:");
while (!/^\d+$/.test(x)) {
  alert("Це не число!");
  x = +prompt("Введіть будь-яке число х:");
}
let y = +prompt("Введіть будь-яке число y:");
while (!/^\d+$/.test(y)) {
  alert("Це не число!");
  y = +prompt("Введіть будь-яке число y:");
}
let typeOfMathOperation = prompt("Введіть вид математичної операції: +, -, *, /");
while (!/^[+\-*/]{1}$/.test(typeOfMathOperation)) {
  alert("Будь ласка, введіть лише один з цих сиволів: +, -, *, /");
  typeOfMathOperation = prompt("Введіть вид математичної операції: +, -, *, /");
}

function calc(x, y, typeOfMathOperation) {
  let result = 0;
  switch(typeOfMathOperation) {
    case "+":
      result = x + y;
      break;
    case "-":
      result = x - y;
      break;
    case "*":
      result = x * y;
      break;
    case "/":
      result = x / y;
      break;
  }
  return result;
}
console.log(calc(x, y, typeOfMathOperation));